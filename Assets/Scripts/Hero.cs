﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;


public class Hero : FarmCharacter
{

    List<Animal> followers;
    public ReactiveProperty<int> points { get; private set; }

    public float unloadTime = 0.5f;

    public int capacity = 5;

    public int maxTime = 30;

    public int t1 = 10;

    float intervalTime;

    public ReactiveProperty<float> time { get; private set; }

    public IReadOnlyReactiveProperty<bool> timeIsUp { get; private set; }


    [Inject]
    Animal.Pool prefabPool;

    //public ReactiveProperty<Vector3> velocity { get; private set; }

    // Start is called before the first frame update
    protected new void Start()
    {
        base.Start();

        var clickStream = Observable.EveryUpdate()
            .Where(_ => Input.GetMouseButtonDown(0))
            .Select(_ => Input.mousePosition);

        clickStream.Subscribe(xs => Move(xs));

        followers = new List<Animal>();
        points = new ReactiveProperty<int>(0);
        time = new ReactiveProperty<float>(0.0f);
        timeIsUp = time.Select(x => x >= maxTime).ToReactiveProperty();

    }

    public void Restart()
    {
        foreach (Animal animal in followers)
        {
            animal.Stop();
            prefabPool.Despawn(animal);
        }
        speed = defaultSpeed;
        followers = new List<Animal>();
        points.Value = 0;
        time.Value = 0;
        target = Vector3.zero;
        transform.position = Vector3.zero;
        intervalTime = 0;
    }

    // Update is called once per frame
    protected new void Update()
    {
        if (!timeIsUp.Value)
        {
            base.Update();
            if (Vector3.Distance(transform.position, target) < 0.01f)
            {
                velocity = Vector3.zero;
                foreach (Animal follower in followers)
                {
                    follower.velocity = this.velocity;
                }
            }
            time.Value += Time.deltaTime;
            intervalTime += Time.deltaTime;
            if (intervalTime >= t1)
            {
                this.speed += 1;
                intervalTime = 0;
            }
        }
    }

    public void AddFollower(Animal animal)
    {
        if (followers.Count < capacity)
        {
            animal.state = AnimalState.Taken;
            followers.Add(animal);
            if (animal.GetSpeed() < this.speed)
                this.speed = animal.GetSpeed();
            animal.SetVelocity(this.velocity);
            animal.SetSpeed(this.speed);

        }
    }


    public int GetFollowersCount()
    {
        return followers.Count;
    }


    void Move(Vector3 t)
    {
        if (!timeIsUp.Value)
        {
            target = Camera.main.ScreenToWorldPoint(new Vector3(t.x, t.y, 0));
            target = new Vector3(target.x, target.y, 0);
            Vector3 direction = target - transform.position;
            direction.Normalize();
            velocity = new Vector3(direction.x, direction.y, 0);//new ReactiveProperty<Vector3>(direction * speed);

            foreach (Animal animal in followers)
            {
                animal.SetVelocity(this.velocity);
                animal.SetSpeed(this.speed);
            }
        }
    }



    public void Unload()
    {
        if (followers.Count > 0)
        {
            Observable.FromCoroutine(UnloadRoutine)
                      .Subscribe(_ => FinishUnload());
        }
    }

    IEnumerator UnloadRoutine()
    {
        foreach (Animal farmCharacter in followers)
        {
            farmCharacter.Stop();
        }
        this.speed = 0;
        Debug.Log("Unload begins");
        yield return new WaitForSeconds(unloadTime);
        Debug.Log("Unloaded");
    }

    void FinishUnload()
    {
        if (!timeIsUp.Value)
        {
            foreach (Animal farmCharacter in followers)
            {
                prefabPool.Despawn(farmCharacter);
                points.Value += farmCharacter.points;
            }
            followers.Clear();
            this.speed = defaultSpeed;
        }
    }
}
