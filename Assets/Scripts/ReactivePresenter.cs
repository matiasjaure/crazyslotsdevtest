﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ReactivePresenter : MonoBehaviour
{
    public Text points;
    public Text time;

    public GameObject finalPanel;

    [Inject]
    Hero hero;


    // Start is called before the first frame update
    void Start()
    {
        hero.points.SubscribeToText(points, p => "Points: " + p);
        hero.time.SubscribeToText(time, t => "Remaining Time: " + Mathf.CeilToInt(hero.maxTime - t));
        hero.timeIsUp.Where(timeIsUp => timeIsUp == true)
            .Subscribe(_ =>
            {
                finalPanel.SetActive(true);
            });

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Restart()
    {
        finalPanel.SetActive(false);
        hero.Restart();

    }
}
