﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;
public enum AnimalState
{
    Free,
    Taken
}
public class Animal : FarmCharacter
{

    public AnimalState state;

    public int points = 1;


    Vector3 originalPosition;

    public int patrolLenght = 1;

    // Start is called before the first frame update
    protected new void Start()
    {
        base.Start();
        state = AnimalState.Free;
    }

    protected new void Update()
    {
        if (state == AnimalState.Free)
        {
            Patrol();
        }

        base.Update();

    }


    [Inject]
    public void Construct()
    {
        base.Start();
        this.OnTriggerEnter2DAsObservable()
        .Select(collider => collider.gameObject)
        .Subscribe(other => OnTrigger(other));
    }

    void OnTrigger(GameObject other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Hero>().AddFollower(this);
        }
    }

    int patrolDirection = 1;

    void Patrol()
    {

        if (Vector3.Distance(transform.position, target) < 0.01f)
        {
            patrolDirection *= -1;
            SetPatrolTarget();
        }
    }

    void Reset(Vector3 position)
    {
        originalPosition = position;
        transform.position = position;
        state = AnimalState.Free;
        SetPatrolTarget();
    }

    void SetPatrolTarget()
    {
        target = new Vector3(originalPosition.x + (patrolLenght * patrolDirection), originalPosition.y, 0);
        Vector3 direction = target - transform.position;
        direction.Normalize();
        velocity = new Vector3(direction.x, direction.y, 0);
    }

    public class Pool : MonoMemoryPool<Vector3, Animal>
    {
        protected override void Reinitialize(Vector3 position, Animal animal)
        {
            animal.Reset(position);
        }

    }




}
