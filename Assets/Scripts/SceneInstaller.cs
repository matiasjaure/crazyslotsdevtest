using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    public List<GameObject> AnimalPrefabs;

    public GameObject hero;

    public int poolSize = 10;
    public override void InstallBindings()
    {

        Container.Bind<Hero>().FromComponentOn(hero).AsSingle();


        Container.BindMemoryPool<Animal, Animal.Pool>()
            .WithInitialSize(poolSize)
            .FromComponentInNewPrefab(AnimalPrefabs[Random.Range(0, AnimalPrefabs.Count)])
            .UnderTransformGroup("Animals");

    }
}