﻿using System;
using System.Collections;
using System.Collections.Generic;

using UniRx;
using UniRx.Triggers;
using UnityEngine;
public class Fence : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.OnTriggerEnter2DAsObservable()
        // this.OnCollisionEnter2DAsObservable()
        .Select(collider => collider.gameObject)
        .Subscribe(other => OnTrigger(other));
    }


    // Update is called once per frame
    void Update()
    {

    }

    void OnTrigger(GameObject other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Hero>().Unload();
        }
    }
}
