﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FarmCharacter : MonoBehaviour
{

    public float defaultSpeed = 1;

    protected float speed;

    public Vector3 velocity;

    public Vector3 target;


    [Inject]
    Hero hero;

    // Start is called before the first frame update
    protected void Start()
    {
        speed = defaultSpeed;
    }

    // Update is called once per frame
    protected void Update()
    {
        if (!hero.timeIsUp.Value)
        {
            if (velocity != Vector3.zero)
            {
                transform.position += velocity * speed * Time.deltaTime;
            }
        }
    }

    public void SetVelocity(Vector3 velocity)
    {
        this.velocity = velocity;
    }

    public float GetSpeed()
    {
        return this.speed;
    }
    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }
    public void Stop()
    {
        this.velocity = Vector3.zero;
        this.speed = defaultSpeed;
    }
}
