﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class AnimalManager : MonoBehaviour
{
    public float minX, maxX, minY, maxY;

    [Inject]
    Animal.Pool animalPool;

    [Inject]
    Hero hero;

    public void Start()
    {

    }

    public void Update()
    {
        if (ShouldSpawn())
        {
            animalPool.Spawn(new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0));
        }
    }

    bool ShouldSpawn()
    {
        return (animalPool.NumActive - hero.GetFollowersCount() < 10);
    }




}
